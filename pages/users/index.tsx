import { useRouter } from 'next/router';
import Layout from '../../components/Layout';

interface UsersProps {
    dataUsers: Array<any>;
}
export default function Users(props: UsersProps) {
    const { dataUsers } = props;
    const router = useRouter();

    // console.log("test data user", dataUsers);
    return (
        <Layout pageTitle=" User Pages">
            {dataUsers.map((user, index) => (
                <div key={user.id} onClick={() => router.push(`/users/${user.id}`)}>
                    <p>{user.name}</p>
                    <p>{user.email}</p>
                </div>
            ))}
        </Layout>
    )
}

export async function getStaticProps() {
    const res = await fetch('https://jsonplaceholder.typicode.com/users');
    const dataUsers = await res.json();
    return {
        props: {
            dataUsers: dataUsers,
        },
    };
}