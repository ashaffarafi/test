import react from 'react';
import Footer from "../Footer";
import Header from "../Header";
import styles from "./Layout.module.css";
import Head from 'next/Head';

interface LayoutProps {
    children: react.ReactNode;
    pageTitle: string;
}
export default function Layout(props: LayoutProps) {
    const { children, pageTitle } = props;
    return (
        <>
            <Head>
                <title>
                    Next Basic |
                    {''}
                    {pageTitle}
                </title>
                <meta name="description" content="Website Next Basic" />
            </Head>
            <div className={styles.container}>
                <Header />
                <div className={styles.content}>{children}</div>
                <Footer />
            </div>
        </>
    );
}
